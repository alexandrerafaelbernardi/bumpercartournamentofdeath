﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu1 : MonoBehaviour
{
    public static bool GameIsPaused = false;

	public GameObject PauseMenuUI;
	public GameObject Image1;

	void Start() {
		GameIsPaused = false;
		Time.timeScale = 1f;
	}

    void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused) 
            {
                Resume();
            } else {
                Pause();
            }
        }
    }

    public void Resume() {
		PauseMenuUI.SetActive(false);
		Image1.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause() {
		PauseMenuUI.SetActive(true);
		Image1.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        
    }

    public void Reiniciar() {
        GameIsPaused = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene("Game");
        int y = SceneManager.GetActiveScene().buildIndex;
        SceneManager.UnloadSceneAsync(y);
    }

    public void abrirMenu(){
        SceneManager.LoadScene("Menu");
        int y = SceneManager.GetActiveScene().buildIndex;
        SceneManager.UnloadSceneAsync(y);
    }

    public void fecharJogo(){
        Application.Quit();
    }

}

