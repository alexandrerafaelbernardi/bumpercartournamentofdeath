﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    private int itemInv = 0;
	private float contagem;

	public GameObject inventario;
	public GameObject imageInventario;
    public bool municaoInf;
	public int quantidadeBulita;
	public int quantidadeFogo;
	public int quantidadeOleo;
	public int quantidadeChicle;

	public Transform FrontPoint;
	public Transform BackPoint;
	public GameObject BulitaPrefab;
	public GameObject FogoPrefab;
	public GameObject ChiclePrefab;
	public GameObject OleoPrefab;
	public GameObject TextoInv;
	public Sprite BodoqueImage;
	public Sprite LançaChamasImage;
	public Sprite ChicleImage;
	public Sprite BarrilDeOleoImage;
	public Sprite Vazio;

	public float force = 20f;

	public bool flagSpace = true;

    //Lista de itens
    //0 - nenhum
    //1 - bodoque       // pode ser disparado 3 vezes long range
    //2 - lança-chama   // pode disparadas 6 vezes short range
    //3 - óleo          // pode ser usado uma vez
    //4 - chiclé        // pode ser usado uma vez

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
			switch (itemInv) {
			case 1:     
				if (quantidadeBulita > 0) {
					GameObject Bulita  = Instantiate (BulitaPrefab, FrontPoint.position, FrontPoint.rotation);
					Bulita.name = "Bulita";
					Rigidbody2D rb = Bulita.GetComponent<Rigidbody2D> ();
					rb.AddForce (FrontPoint.up * force, ForceMode2D.Impulse);
					
                    if(!municaoInf)
                        quantidadeBulita--;

					if (quantidadeBulita == 0) {
						itemInv = 0;
						imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = Vazio;
						inventario.SetActive(false);
					}
					TextoInv.GetComponent<UnityEngine.UI.Text>().text = quantidadeBulita + "x";
                        
				}
                break;

			case 3:
				GameObject Oleo = Instantiate (OleoPrefab, BackPoint.position, BackPoint.rotation);
				Oleo.name = "Oleo";
				Rigidbody2D ro = Oleo.GetComponent<Rigidbody2D> ();
				itemInv = 0;
				inventario.SetActive(false);
				imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = Vazio;
				inventario.SetActive(false);
				break;

			case 4:
				GameObject Chicle = Instantiate (ChiclePrefab, BackPoint.position, BackPoint.rotation);
				Chicle.name = "Chiclete";
				Rigidbody2D rc = Chicle.GetComponent<Rigidbody2D> ();
				itemInv = 0;
				imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = Vazio;
				inventario.SetActive(false);
				break;
        	}     
        }

		if (Input.GetKey("space"))
        {
			if(itemInv==2){
				FogoPrefab.SetActive(true);
				
				contagem -= Time.deltaTime;

				TextoInv.GetComponent<UnityEngine.UI.Text>().text = contagem + "s";

				if(contagem <= 0f){
					Debug.Log("acabou o fogo");
					itemInv = 0;
					imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = Vazio;
					inventario.SetActive(false);
					FogoPrefab.SetActive(false);
				}

				if(municaoInf)
					contagem = 3;
				
			}		
        }else{
			FogoPrefab.SetActive(false);
		}
    }

    //identificar quando o jogador encostar
    void OnTriggerEnter2D(Collider2D item){

        switch(item.name){
            case "Bodoque":     
                if(itemInv==0){ // verifico se n tem item no inventario
                    itemInv = 1;
					quantidadeBulita = 3;
					inventario.SetActive(true);
					imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = BodoqueImage;
					if(flagSpace)
						TextoInv.GetComponent<UnityEngine.UI.Text>().text = quantidadeBulita + "x";
                    item.transform.gameObject.SetActive(false);
                    }
                break;

            case "Lança-chamas":
                if(itemInv==0){
					contagem = 3f; //3 segundos
                    itemInv = 2;
					inventario.SetActive(true);
					imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = LançaChamasImage;
					TextoInv.GetComponent<UnityEngine.UI.Text>().text = contagem + "s";
                    item.transform.gameObject.SetActive(false);
                }
                break;

            case "BarrilDeOleo":
                if(itemInv==0){
                    itemInv = 3;
					quantidadeOleo = 1;
					inventario.SetActive(true);
					imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = BarrilDeOleoImage;
					TextoInv.GetComponent<UnityEngine.UI.Text>().text = quantidadeOleo + "x";
                    item.transform.gameObject.SetActive(false);
                }
                break;

            case "Chicle":
                if(itemInv==0){
                    itemInv = 4;
					quantidadeChicle = 1;
					inventario.SetActive(true);
					imageInventario.GetComponent<UnityEngine.UI.Image>().sprite = ChicleImage;
					TextoInv.GetComponent<UnityEngine.UI.Text>().text = quantidadeChicle + "x";
                    item.transform.gameObject.SetActive(false);
                }
                break;
        }      
    }
}
