﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimentação : MonoBehaviour
{

	public float maxSpeed;
    public float minimunSpeed;
	public float turnSpeed;
	public float cadenciaDeAumentoDeVelocidade;

	private bool inerciaUp;
	private bool inerciaDown;
	private float actualSpeed;
    private float targetAngle;

	public GameObject Acabou;
	public GameObject Image1;
	public GameObject Inventario;
	public GameObject Ganhou;
	public GameObject Enemy1;
	public GameObject Enemy2;
	public GameObject Enemy3;
	public GameObject MensagemMorte;

	//private List<Transform> congaLine = new List<Transform>();

    void Update()
    {
		if (Input.GetKey("up")){ // ir acelerando até certo ponto

			inerciaUp = true;

			if(actualSpeed>maxSpeed) // travo para nao ganhar mais aceleracao
				actualSpeed = maxSpeed;
			else
				actualSpeed = actualSpeed + cadenciaDeAumentoDeVelocidade * Time.deltaTime; // aumento gradativamente a velocidade

			transform.Translate(Vector2.up * actualSpeed * Time.deltaTime);

		}else if (Input.GetKey("down")){

			inerciaDown = true;

			if(actualSpeed>maxSpeed)
				actualSpeed = maxSpeed;
			else
				actualSpeed = actualSpeed + cadenciaDeAumentoDeVelocidade * Time.deltaTime;

			transform.Translate(Vector2.down * actualSpeed * Time.deltaTime);

		}else{
			if(inerciaUp){
				if(actualSpeed<=minimunSpeed){
					actualSpeed = minimunSpeed;
					inerciaUp = false;
				}else
					actualSpeed = actualSpeed - cadenciaDeAumentoDeVelocidade * Time.deltaTime;

				transform.Translate(Vector2.up * actualSpeed * Time.deltaTime);
			}
			if(inerciaDown){
				if(actualSpeed<=minimunSpeed){
					actualSpeed = minimunSpeed;
					inerciaDown = false;
				}else
					actualSpeed = actualSpeed - cadenciaDeAumentoDeVelocidade * Time.deltaTime;

				transform.Translate(Vector2.down * actualSpeed * Time.deltaTime);
			}
		}
			
		
		// Rotação do jogador
		if (Input.GetKey("left"))
			transform.Rotate(Vector3.forward * turnSpeed * Time.deltaTime);

        if (Input.GetKey("right"))
			transform.Rotate(Vector3.back * turnSpeed * Time.deltaTime);

		if (Enemy1.activeSelf == false && Enemy2.activeSelf == false && Enemy3.activeSelf == false) {
			GanhouFuncao();
		}
    }

	public void Morreu(){
		Debug.Log ("TESTEMORREU");
		Time.timeScale = 0f;
		Acabou.SetActive(true);
		Image1.SetActive(true);
		Inventario.SetActive(false);
		MensagemMorte.SetActive(true);
	}

	public void abrirPauseMenu(){
		Time.timeScale = 0f;
		Acabou.SetActive(true);
		Image1.SetActive(true);
		Inventario.SetActive(false);
	}

	public void GanhouFuncao(){
		Ganhou.SetActive(true);
		Inventario.SetActive(false);
		Time.timeScale = 0f;
	}
}
    