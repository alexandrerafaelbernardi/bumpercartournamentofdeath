﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//video referencias para animações de dano/morte
//https://www.youtube.com/watch?v=YHd-4Zb3Xbg

public class EnemyHitBox : MonoBehaviour
{
    Image myImageComponent;

    public int vida;
    public int tempoDeImortabilidade;
    public Animator animator;
    public GameObject coracao1;
    public GameObject coracao2;
    public GameObject coracao3;

    //imagens dos coracoes
    public Sprite imgCoracaoEmpty;
    public Sprite imgCoracaoHalf;
    public Sprite imgCoracaoFull;

    //gameobject enemy
    public GameObject Enemy;

    private bool damageFlag = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //identificar quando o jogador encostar do cubo
    void OnTriggerEnter2D(Collider2D hitbox){
		if (damageFlag) {
			if (hitbox.name == "HitBoxPlayer") {   
				CausarDano ();
			}
			if (hitbox.name == "Bulita") {
				CausarDano ();
			}
			if (hitbox.name == "FireDamage")
				CausarDano ();
		}
    }

    private void CausarDano(){
        //aplica 1 de dano
        vida--;
        damageFlag = false;

        if(vida<= 0){
            animator.SetTrigger("Morrendo"); // chamo a animação de dan
        }else{
            animator.SetTrigger("Dano"); // chamo a animação de dano
        }

        //trocar sprite de acordo com a quantia de vida
        //coração 3 = 6/5
        //coração 2 = 4/3
        //coração 1 = 2/1
        
        //incialmente os 3 sprites sao o coracao cheio
        switch(vida){
            case 5:
                //coracao 3 = meio coracao
                //coracao 2 = coracao cheio
                //coracao 1 = coracao cheio
                coracao3.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoHalf;
                coracao2.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoFull;
                coracao1.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoFull;
                break;
            case 4:
                //coracao 3 = coracao vazio
                //coracao 2 = coracao cheio
                //coracao 1 = coracao cheio
                coracao3.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao2.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoFull;
                coracao1.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoFull;
                break;
            case 3:
                //coracao 3 = coracao vazio
                //coracao 2 = meio coracao
                //coracao 1 = coracao cheio
                coracao3.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao2.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoHalf;
                coracao1.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoFull;
                break;
            case 2:
                //coracao 3 = coracao vazio
                //coracao 2 = coracao vazio
                //coracao 1 = coracao cheio
                coracao3.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao2.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao1.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoFull;
                break;
            case 1:
                //coracao 3 = coracao vazio
                //coracao 2 = coracao vazio
                //coracao 1 = meio coracao
                coracao3.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao2.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao1.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoHalf;
                break;
            case 0:
                //coracao 3 = coracao vazio
                //coracao 2 = coracao vazio
                //coracao 1 = coracao vazio
                coracao3.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao2.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                coracao1.GetComponent<UnityEngine.UI.Image>().sprite = imgCoracaoEmpty;
                break;
            default: break;
        }

		StartCoroutine(DelayBeforeChase()); // chamo a thread para contar 3 segundos após o dano
    }


    IEnumerator DelayBeforeChase(){
        yield return new WaitForSeconds (tempoDeImortabilidade);
        damageFlag = true; // libero para receber dano novamente
    }

}