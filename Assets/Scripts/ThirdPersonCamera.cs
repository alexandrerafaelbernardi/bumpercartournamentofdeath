﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour
{
    

    public Transform lookAt;
    public Transform camTransform;

    private Camera cam;

    private float distance = 0.0f;
    private float currentX = 90.0f;
    private float currentY = 0.0f;

    private void Start(){
        camTransform = transform;
        cam = Camera.main;

    }

    private void Update(){
    }

    private void LateUpdate(){
        Vector3 dir = new Vector3(10, 5, distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        camTransform.position = lookAt.position + rotation * dir;
        camTransform.LookAt(lookAt.position);
    }
}