﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projetil : MonoBehaviour
{
	public GameObject Bulita;

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.name == "HurtBox" || collision.name == "ColiderTop" || collision.name == "ColiderDown" || collision.name == "ColiderLeft"|| collision.name == "ColiderRight") {
			Destroy(Bulita);
		}

	}	
}
