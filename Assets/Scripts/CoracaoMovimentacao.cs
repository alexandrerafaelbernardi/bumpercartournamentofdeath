﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoracaoMovimentacao : MonoBehaviour
{

    public Vector3 rotacao;
    void Update(){
        transform.rotation = Quaternion.Euler(rotacao);
    }
}
