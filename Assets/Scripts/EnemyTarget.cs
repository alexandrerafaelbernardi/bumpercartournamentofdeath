﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTarget : MonoBehaviour
{
    public bool parado;
    public float moveSpeed;
	public float turnSpeed;
	public Transform toGo;
	private Transform Aux;
    public Transform AcaoDoOleo;
	private Vector2 moveDirection;
	public int passou;
	public int tempoChicle;

	void Start(){
		passou = 0;
		tempoChicle = 3;
	}

    void Update()
    {
		if (parado || passou == 1) {
			Aux = this.transform;
		} else {
			if (!parado && passou == 0) {
				Aux = toGo;
			} else {
				if (!parado && passou == 2) {
					Aux = AcaoDoOleo;
				}
			}
		}

        Vector2 currentPosition = transform.position;
		
		Vector2 moveTowards = Aux.position;

        moveDirection = moveTowards - currentPosition;
        moveDirection.Normalize();

		Vector2 target = moveDirection + currentPosition;
		transform.position = Vector3.Lerp (currentPosition, target, moveSpeed * Time.deltaTime);

		float targetAngle = Mathf.Atan2 (moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;

        targetAngle = targetAngle - 90;

		transform.rotation = Quaternion.Lerp (transform.rotation, 
		                                       Quaternion.Euler (0, 0, targetAngle), 
		                                       turnSpeed * Time.deltaTime);
    }

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.name == "Chiclete") {
			passou = 1;
			StartCoroutine(DelayBeforeChase());
		}
		if (collision.name == "Oleo") {
			passou = 2;
			StartCoroutine(DelayBeforeChase());
		}
	}

	IEnumerator DelayBeforeChase(){
		yield return new WaitForSeconds (tempoChicle);
		passou = 0; // libero para receber dano novamente

	}

	public void desativarEnemy(){
		this.transform.gameObject.SetActive(false);
	}
}
    