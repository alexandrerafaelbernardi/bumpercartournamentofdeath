﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnaItem : MonoBehaviour
{
	public GameObject BodoquePrefab;
	public GameObject LançaChamasPrefab;
	public GameObject ChiclePrefab;
	public GameObject BarrilDeOleoPrefab;

	public float respawnTime = 5.0f;

	void Start(){
		RandomRespawnInicial ();
		StartCoroutine(RandomRespawn());

	}

	public void RespawnItem(int item){
		Debug.Log(item);
		switch (item) {
			case 1:     
				GameObject Bodoque = Instantiate (BodoquePrefab) as GameObject;
				Bodoque.name = "Bodoque";
				Bodoque.transform.position = new Vector2 (Random.Range (2.00f, 29.00f), Random.Range (25.00f, 52.99f));
				break;

			case 2:
				GameObject LançaChamas = Instantiate(LançaChamasPrefab) as GameObject;
				LançaChamas.name = "Lança-chamas";
				LançaChamas.transform.position = new Vector2 (Random.Range (2.00f, 29.00f), Random.Range (25.00f, 52.99f));
				break;

			case 3:
				Debug.Log("eNTROU 3");
				GameObject BarrilDeOleo = Instantiate(BarrilDeOleoPrefab) as GameObject;
				BarrilDeOleo.name = "BarrilDeOleo";
				BarrilDeOleo.transform.position = new Vector2 (Random.Range (2.00f, 29.00f), Random.Range (25.00f, 52.99f));
				break;

			case 4:
				Debug.Log("eNTROU 4");
				GameObject Chicle = Instantiate(ChiclePrefab) as GameObject;
				Chicle.name = "Chicle";
				Chicle.transform.position = new Vector2 (Random.Range (2.00f, 29.00f), Random.Range (25.00f, 52.99f));
				break;
			} 
		
	}

	public void RandomRespawnInicial(){
			int aux = Random.Range (1, 5);
			RespawnItem (aux);
	}

	IEnumerator RandomRespawn(){
		while (true) {
			int aux = Random.Range (1, 4);
			yield return new WaitForSeconds(respawnTime);
			Debug.Log(aux);
			RespawnItem (aux);
		}
	}

}
