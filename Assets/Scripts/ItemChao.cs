﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemChao : MonoBehaviour
{
	public GameObject Item;

	void OnTriggerEnter2D(Collider2D collision){
		if (collision.name == "SpriteEnemy1" || collision.name == "SpriteEnemy2" || collision.name == "SpriteEnemy3") {
			Destroy(Item);
		}

	}	
}
